#!/bin/bash
SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

HOST="false"

usage() {
    echo "$0 [OPTIONS]"
    echo ""
    echo "Install configs"
    echo ""
    echo "Options"
    echo "  -h      Install host configuration (default: false)"
    echo ""
}

while getopts "fh" opt; do
    case ${opt} in
        h)
            usage
            exit 0
            ;;
        h)
            HOST="true"
            ;;
    esac
done

mkdir_and_backup() {
    local file=$1
    mkdir -p $(dirname ${file})
    test -f ${file} && mv ${file} ${file}.orig
}

echo "Setting tmux"
mkdir_and_backup $HOME/.tmux.conf
ln -s $SCRIPTPATH/tmux.conf $HOME/.tmux.conf
mkdir -p $HOME/.tmux/plugins
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

echo "Setting bashrc"
mkdir_and_backup $HOME/.bashrc
mkdir_and_backup $HOME/.bashrc_aliases
ln -s $SCRIPTPATH/bashrc $HOME/.bashrc
ln -s $SCRIPTPATH/bashrc_aliases $HOME/.bashrc_aliases
sudo dnf install -y fzf

echo "Setting starship"
curl -sS https://starship.rs/install.sh | sh

echo "Setting gitconfig"
mkdir_and_backup $HOME/.gitconfig
ln -s $SCRIPTPATH/gitconfig $HOME/.gitconfig


mini_progs="vim"
sudo dnf install $mini_progs

if [[ $ALL == "true" ]] then
    echo "Setting kitty"
    mkdir_and_backup $HOME/.config/kitty/kitty.conf
    sudo dnf install -y kitty
    mkdir -p $HOME/.config/kitty/kitty.conf
    ln -s $SCRIPTPATH/kitty.conf $HOME/.config/kitty/kitty.conf
    git clone --depth 1 https://github.com/dexpota/kitty-themes.git ~/.config/kitty/kitty-themes
    ln -s ./kitty-themes/themes/Darkside.conf ~/.config/kitty/theme.conf
    
    echo "Setting hexchat"
    mkdir_and_backup $HOME/hexchat.conf/hexchat.conf
    #sudo dnf install -y hexchat
    mkdir -p $HOME/hexchat.conf
    ln -s $SCRIPTPATH/hexchat.conf $HOME/hexchat.conf/hexchat.conf
    ## Full programs
    my_progs="vim \
        neovim \
        podman \
        gcc \
        clang \
        autoconf \
        libtool \
        ninja-build \
        cmake \
        git \
        fontsawesome-fonts \
        qemu-kvm \
        unzip \
        wget \
        cargo
    "
    sudo dnf install $my_progs

    echo "Install cool programs"
    cargo install --locked --features clipboard broot || true
    mkdir -p ~/.local/share/bash-preexec
    curl https://raw.githubusercontent.com/rcaloras/bash-preexec/master/bash-preexec.sh -o ~/.local/share/bash-preexec/bash-preexec.sh
    cargo install --locked atuin

    ## Fonts
    mkdir -p $HOME/.local/share/fonts
    cd $HOME/.local/share/fonts
    wget https://download.jetbrains.com/fonts/JetBrainsMono-2.304.zip
    unzip JetBrainsMono-2.304.zip
    wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/UbuntuMono.zip
    unzip UbuntuMono.zip
    sudo dnf -y install redhat-*fonts*
    fc-cache -f -v
fi
