#
#Bashrc of Adrian Moreno
#
#Last modified 17/03/2013

#ALIASES
#-------------------------------------------------------------
# The 'ls' family (this assumes you use a recent GNU ls).
#-------------------------------------------------------------
# Add colors for filetype and  human-readable sizes by default on 'ls':
alias ls='ls -lh --color'
alias lx='ls -lXB'         #  Sort by extension.
alias lk='ls -lSr'         #  Sort by size, biggest last.
alias lt='ls -ltr'         #  Sort by date, most recent last.
alias lc='ls -ltcr'        #  Sort by/show change time,most recent last.
alias lu='ls -ltur'        #  Sort by/show access time,most recent last.

# The ubiquitous 'll': directories first, with alphanumeric sorting:
alias ll="ls -lv --group-directories-first"
alias lm='ll |more'        #  Pipe through 'more'
alias lr='ll -R'           #  Recursive ls.
alias la='ll -A'           #  Show hidden files.
alias tree='tree -Csuh'    #  Nice alternative to 'recursive ls' ...



## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'


#Pretty-print some PATH variables:
alias path='echo -e ${PATH//:/\\n}'
alias libpath='echo -e ${LD_LIBRARY_PATH//:/\\n}'


#-----------Handy functions-----------

function extract()      # Handy Extract Program
{
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)   tar xvjf $1     ;;
            *.tar.gz)    tar xvzf $1     ;;
            *.bz2)       bunzip2 $1      ;;
            *.rar)       unrar x $1      ;;
            *.gz)        gunzip $1       ;;
            *.tar)       tar xvf $1      ;;
            *.tbz2)      tar xvjf $1     ;;
            *.tgz)       tar xvzf $1     ;;
            *.zip)       unzip $1        ;;
            *.Z)         uncompress $1   ;;
            *.7z)        7z x $1         ;;
            *)           echo "'$1' cannot be extracted via >extract<" ;;
        esac
    else
        echo "'$1' is not a valid file!"
    fi
}

# TERMCAP manglind automatically to allow 256 capable terminals inside screen
# Source: www.robmeerman.co.uk/unix/256colours
if [ ! -z "$TERMCAP" ] && [ "$TERM" == "screen" ]; then                         
    export TERMCAP=$(echo $TERMCAP | sed -e 's/Co#8/Co#256/g')                  
fi 

# PATH: add $HOME/bin
[ -d "$HOME/bin" ] && export PATH="$PATH:$HOME/bin"
[ -d "$HOME/.local/bin" ] && export PATH="$PATH:$HOME/.local/bin"
[ -d "$HOME/code/go/bin/" ] && export PATH="$PATH:$HOME/code/go/bin"
export PATH="$PATH:/usr/local/go/bin"
export PATH="$PATH:~/.npm-global/bin"

# my vim uses the variable AM_VIM_SIMPLECOLOR to determine the colorscheme to use, add an alias to make it automatic
alias svim='AM_VIM_SIMPLECOLOR=1 && vim'
# If I want to develop (d for dvim), I want full color
alias dvim='AM_VIM_SIMPLECOLOR=0 && vim'
# For a quick edit, do not load the full IDE
alias qvim='AM_VIM_QUICK=0 && vim'

# Let's get really geeky:
set -o vi

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bashrc_aliases ]; then
    . ~/.bashrc_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


#256 colors please:
[[ $COLORTERM = gnome-terminal && ! $TERM = screen-256color ]] && TERM=xterm-256color

export LC_ALL="en_US.UTF-8"

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/amorenoz/dev/gcloud/google-cloud-sdk/path.bash.inc' ]; then . '/home/amorenoz/dev/gcloud/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/amorenoz/dev/gcloud/google-cloud-sdk/completion.bash.inc' ]; then . '/home/amorenoz/dev/gcloud/google-cloud-sdk/completion.bash.inc'; fi

which nvim &> /dev/null && EDITOR=/usr/bin/nvim || EDITOR=/usr/bin/vim
export EDITOR

# PS1
# Minimalistic PS1
export PS1="\n\[$(tput sgr0)\]\[\033[38;5;208m\][\[$(tput bold)\]\[$(tput sgr0)\]\[\033[38;5;33m\]\w\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;202m\]]\[$(tput sgr0)\]\[\033[38;5;15m\]\n \[$(tput sgr0)\]\[\033[38;5;200m\]\A\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;34m\]>>\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

export PS1
export PS2=">> "

## Use starship if available
test -n "$(which starship)" && eval "$(starship init bash)"

# Languages
## Rust
test -d $HOME/.cargo && source "$HOME/.cargo/env"
#alias rust-analyzer='rustup run nightly rust-analyzer'

## Golang
export GOPATH=$HOME/go

#Rust
export PATH="$PATH:$HOME/.cargo/bin"

# History
## Default: history autocomplete takes already written words into consideration
if [[ $- == *i* ]]
then
    bind '"\e[A": history-search-backward'
    bind '"\e[B": history-search-forward'
fi

. "$HOME/.cargo/env"

source /home/amorenoz/.config/broot/launcher/bash/br

# Atuin works fine. Ble.sh vim mode is not, using bash-preexec.
if [[ -f ~/.local/share/bash-preexec/bash-preexec.sh ]]; then
##    source ~/.local/share/blesh/ble.sh
    source ~/.local/share/bash-preexec/bash-preexec.sh
    which atuin &> /dev/null && eval "$(atuin init bash)"
else
    ## Use fzf if available
    test -f /usr/share/fzf/shell/key-bindings.bash && source /usr/share/fzf/shell/key-bindings.bash
fi
